using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace _4
{
    class Program
    {
        static private IEnumerable<IDictionary<string, string>> readRecords()
        {
            Regex r = new Regex(@"(?<tag>[a-z]{3}):(?<value>[^ ]+)");
            string? line = null;
            do
            {
                List<string> recordList = new List<string>();
                do
                {
                    line = Console.ReadLine();
                    recordList.Add(line);
                }
                while (!string.IsNullOrEmpty(line));
                string record = string.Join(" ", recordList);
                MatchCollection x = r.Matches(record);
                yield return x.ToDictionary((m) => m.Groups["tag"].Value, (m) => m.Groups["value"].Value);
            }
            while (!(line is null));
        }

        static bool validateHeight(string v)
        {
            Match m = new Regex(@"^(?<n>[0-9]+)(?<u>(?:cm)|(?:in))$").Match(v);
            _ = int.TryParse(m.Groups["n"].Value, out int intV);
            switch (m.Groups["u"].Value)
            {
                case "cm":
                    return intV >= 150 && intV <= 193;
                case "in":
                    return intV >= 59 && intV <= 76;
                default:
                    return false;
            }
        }

        static bool validateValue(string k, string v)
        {
            _ = int.TryParse(v, out int intV);
            return k switch {
                "byr" => intV >= 1920 && intV <= 2002,
                "iyr" => intV >= 2010 && intV <= 2020,
                "eyr" => intV >= 2020 && intV <= 2030,
                "hgt" => validateHeight(v),
                "hcl" => Regex.IsMatch(v, @"^#[0-9a-f]{6}$"),
                "ecl" => new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" }.Contains(v),
                "pid" => Regex.IsMatch(v, @"^[0-9]{9}$"),
                _ => true,
            };
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            bool validateRecord(IDictionary<string, string> dict)
            {
                var keys = dict.Keys;
                if (!keys.Contains("byr")
                 || !keys.Contains("iyr")
                 || !keys.Contains("eyr")
                 || !keys.Contains("hgt")
                 || !keys.Contains("hcl")
                 || !keys.Contains("ecl")
                 || !keys.Contains("pid")) return false;
                foreach ((string k, string v) in dict)
                {
                    Console.WriteLine($"> '{k}','{v}'");
                    if (!validateValue(k, v)) return false;
                    Console.WriteLine($"  => valid");
                }
                return true;
            }
            int validRecords = readRecords().Where((dict) => validateRecord(dict)).Count();

            Console.WriteLine($"{validRecords} valid records found.");
        }
    }
}
