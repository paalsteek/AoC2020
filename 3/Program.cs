﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _3
{
    using Map = List<List<bool>>;

    class Program
    {
        static void PrintMap(Map m)
        {
            foreach (List<bool> row in m)
            {
                foreach (bool column in row)
                {
                    if (column)
                    {
                        Console.Write('#');
                    }
                    else
                    {
                        Console.Write('.');
                    }
                }
                Console.Write(Console.Out.NewLine);
            }
        }

        static int FindTrees(Map map, int s1, int s2)
        {
            int rows = map.Count;
            int columns = map[0].Count;
            Console.WriteLine($"Map: {rows}x{columns}");
            int r = 0;
            int c = 0;
            int trees = 0;
            while (r < rows)
            {
                if (map[r][c])
                {
                    trees++;
                }
                r += s1;
                c = (c + s2) % columns;
            }
            return trees;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Map map = new Map();
            string line = Console.ReadLine();
            while (!(line is null))
            {
                map.Add(new List<bool>(line.Select((c) => c == '#')));
                line = Console.ReadLine();
            }
            PrintMap(map);
            int t = FindTrees(map, 1, 3);
            decimal sum = t;
            Console.WriteLine($"Hit {t} trees.");
            t = FindTrees(map, 1, 1);
            sum *= t;
            Console.WriteLine($"Hit {t} trees.");
            t = FindTrees(map, 1, 5);
            sum *= t;
            Console.WriteLine($"Hit {t} trees.");
            t = FindTrees(map, 1, 7);
            sum *= t;
            Console.WriteLine($"Hit {t} trees.");
            t = FindTrees(map, 2, 1);
            sum *= t;
            Console.WriteLine($"Hit {t} trees.");
            Console.WriteLine($"Total: {sum}");
        }
    }
}
