using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc
{
    class Program
    {
        static IEnumerable<int> readValues()
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (string.IsNullOrEmpty(line)) yield break;
                yield return int.Parse(line);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            List<int> adapters = new List<int>(readValues());
            adapters.Add(0);
            adapters.Sort();
            (int, int, int) countJolts((int last, int j1, int j3) x, int v) { 
                return (v, ((v - x.last) == 1) ? x.j1+1 : x.j1, ((v - x.last) == 3) ? x.j3+1 : x.j3);
            }
            (int _, int j1, int j3) = adapters.Aggregate((0, 0, 1), countJolts);
            Console.WriteLine($"{j1} * {j3} = {j1*j3}");
            long combinations = 1;
            for (int i = 0; i < adapters.Count - 1; i++)
            {
                int contCount = 1;
                while (i < adapters.Count - 1 && adapters[i+1] - adapters[i] == 1)
                {
                    i++;
                    contCount++;
                }
                //Console.WriteLine($"> {i}, {adapters[i]}, {contCount}");
                combinations = combinations * contCount switch
                    {
                        1 => 1,
                        2 => 1,
                        3 => 2,
                        4 => 4,
                        5 => 7,
                        6 => 12,
                        _ => throw new NotImplementedException()
                    };
                if (i < adapters.Count - 1 && adapters[i+1] - adapters[i] != 3) throw new NotImplementedException("Gap not 3");
            }
            Console.WriteLine($"There are {combinations} possible combinations.");
        }
    }
}
