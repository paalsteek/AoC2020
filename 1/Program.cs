﻿using System;
using System.Collections.Generic;

namespace _1
{
    class Program
    {
        public HashSet<int> Values { get; private set; } = new HashSet<int>();

        int findMatch(IEnumerable<int> list, int target = 2020)
        {
            foreach (int n in list)
            {
                if (Values.Contains(target-n)) return n*(target-n);
                Values.Add(n);
            }
            throw new Exception("No matching values found!");
        }

        int findMatch()
        {
            foreach (int n in Values)
            {
                try
                {
                    return findMatch(Values, 2020-n)*n;
                }
                catch (Exception)
                {
                }
            }
            throw new Exception("No matching values found!");
        }

        Program(IEnumerable<int> list)
        {
            Values = new HashSet<int>(list);
        }

        static IEnumerable<int> readValues()
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (line is null) break;
                yield return int.Parse(line);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var x = new Program(readValues());
            Console.WriteLine($"Found: {x.findMatch()}");
        }
    }
}
