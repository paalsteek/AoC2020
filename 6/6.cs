using System;
using System.Collections.Generic;
using System.Linq;

namespace _5
{
    public static class HashSetExt
    {
        public static void Append<T>(this HashSet<T> dest, IEnumerable<T>? values)
        {
            if (values is null) return;

            foreach (T v in values)
            {
                dest.Add(v);
            }
        }
    }

    static class Program
    {
        static private IEnumerable<IList<string>> readRecords()
        {
            string? line = null;
            do
            {
                IList<string> answerSet = new List<string>();
                do
                {
                    line = Console.ReadLine();
                    if (!string.IsNullOrEmpty(line)) answerSet.Add(line);
                }
                while (!string.IsNullOrEmpty(line));
                Console.WriteLine($"Answers: {string.Join(",", answerSet)}");
                yield return answerSet;
            }
            while (!(line is null));
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int sum = readRecords().Select((record) => {
                    if (record.Count == 0) return 0;
                    Console.WriteLine($"record: {string.Join(",", record)}");
                    return record.Aggregate(null, (HashSet<char>? aggr, string entry) => {
                                if (aggr is null) return new HashSet<char>(entry);
                                else aggr.IntersectWith(entry);
                                return aggr;
                            })?.Count() ?? 0;
                }).Sum();
            Console.WriteLine($"Sum: {sum}");
        }
    }
}
