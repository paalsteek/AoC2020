using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc
{
    class Program
    {
        static IEnumerable<long> readValues()
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (string.IsNullOrEmpty(line)) yield break;
                yield return long.Parse(line);
            }
        }

        static bool isSumOfTwo(long v, Queue<long> preamble)
        {
            foreach(long p1 in preamble)
            {
                if ((v-p1 != p1) && preamble.Contains(v-p1))
                {
                    //Console.WriteLine($"{v} is a sum of {p1} and {v-p1}");
                    return true;
                }
            }
            return false;
        }

        static long findWeakness(IEnumerable<long> values, int preambleSize)
        {
            Queue<long> preamble = new Queue<long>(preambleSize);
            foreach (long v in values)
            {
                if (preamble.Count >= preambleSize)
                {
                    if (!isSumOfTwo(v, preamble))
                    {
                        return v;
                    }
                    _ = preamble.Dequeue();
                }
                preamble.Enqueue(v);
            }
            throw new Exception("No weakness found.");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int preambleSize = 25;
            if (args.Count() > 0) preambleSize = int.Parse(args[0]);
            List<long> values = new List<long>(readValues()); // The convertion to List ensures that all values are read.
            long weakness = findWeakness(values, preambleSize);
            Console.WriteLine($"{weakness} is not a sum of two values in the preamble.");
            for (int i = 0; i < values.Count; i++)
            {
                List<long> contSet = new List<long>(values.Count - i);
                contSet.Add(values[i]);
                long sum = values[i];
                for (int j = i+1; j < values.Count; j++)
                {
                    sum += values[j];
                    contSet.Add(values[j]);
                    if (sum > weakness) break;
                    if (sum == weakness)
                    {
                        Console.WriteLine($"Cracked the code: {contSet.Min() + contSet.Max()}");
                        return;
                    }
                }
            }
        }
    }
}
