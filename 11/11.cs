using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc
{
    class Location
    {
        public bool IsSeat { get; }
        public bool IsOccupied { get; set; }

        public Location(char c)
        {
            switch (c)
            {
                case '.':
                    IsSeat = false;
                    IsOccupied = false;
                    break;
                case 'L':
                    IsSeat = true;
                    IsOccupied = false;
                    break;
                case '#':
                    IsSeat = true;
                    IsOccupied = true;
                    break;
                default:
                    throw new NotImplementedException($"Unknown location value {c}");
            }
        }

        public char ToChar()
        {
            if (!IsSeat) return '.';
            if (IsOccupied) return '#';
            return 'L';
        }

        public static IEnumerable<Location> parseRow(string row)
        {
            foreach (char c in row)
            {
                yield return new Location(c);
            }
        }
    }

    class Program
    {
        static IEnumerable<string> readValues()
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (string.IsNullOrEmpty(line)) yield break;
                yield return line;
            }
        }

        static int SafeIsOccupied(List<List<Location>> map, int i, int j)
        {
            return (i >= 0 && i < map.Count && j >= 0 && j < map[0].Count && map[i][j].IsOccupied) ? 1 : 0;
        }

        static int IsOccupiedLine(List<List<Location>> map, int i, int j, int a, int b)
        {
            while (i + a >= 0 && i + a < map.Count && j + b >= 0 && j + b < map[0].Count)
            {
                i += a;
                j += b;
                if (!map[i][j].IsSeat) continue;
                return map[i][j].IsOccupied ? 1 : 0;
            }
            return 0;
        }

        static void PrintMap(List<List<Location>> map)
        {
            foreach (List<Location> row in map)
            {
                foreach (Location l in row)
                {
                    Console.Write(l.ToChar());
                }
                Console.WriteLine();
            }
        }

        static List<List<Location>> CloneMap(List<List<Location>> map)
        {
            List<List<Location>> cMap = new List<List<Location>>(map.Count);
            foreach (List<Location> row in map)
            {
                List<Location> cRow = new List<Location>(row.Count);
                cMap.Add(cRow);
                foreach (Location l in row)
                {
                    cRow.Add(new Location(l.ToChar()));
                }
            }
            return cMap;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            List<List<Location>> map = new List<List<Location>>(readValues().Select((line) => new List<Location>(Location.parseRow(line))));
            bool updated = true;
            List<List<Location>> lastMap = CloneMap(map);
            while (updated)
            {
                //PrintMap(map);
                //Console.WriteLine();
                updated = false;
                for (int i = 0; i < map.Count; i++)
                {
                    for (int j = 0; j < map[0].Count; j++)
                    {
                        Location l = map[i][j];
                        if (l.IsSeat)
                        {
                            int occupiedCount = IsOccupiedLine(lastMap, i, j, -1, -1)
                                              + IsOccupiedLine(lastMap, i, j, -1,  0)
                                              + IsOccupiedLine(lastMap, i, j, -1, +1)
                                              + IsOccupiedLine(lastMap, i, j,  0, -1)
                                              + IsOccupiedLine(lastMap, i, j,  0, +1)
                                              + IsOccupiedLine(lastMap, i, j, +1, -1)
                                              + IsOccupiedLine(lastMap, i, j, +1,  0)
                                              + IsOccupiedLine(lastMap, i, j, +1, +1);
                            //Console.Write(occupiedCount);
                            if (l.IsOccupied && occupiedCount >= 5) { l.IsOccupied = false; updated = true; }
                            else if (!l.IsOccupied && occupiedCount == 0) { l.IsOccupied = true; updated = true; }
                        }
                        //else Console.Write('.');
                    }
                    //Console.WriteLine();
                }
                //Console.WriteLine();
                lastMap = CloneMap(map);
            }
            Console.WriteLine($"Steady state: {map.SelectMany((row) => row).Where((l) => l.IsOccupied).Count()}");
        }
    }
}
