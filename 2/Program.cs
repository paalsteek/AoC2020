﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace _2
{
    class Program
    {

        static bool checkCount(int min, int max, char ch, string password)
        {
            int count = password.Count((c) => c == ch);
            return min <= count && max >= count;
        }

        static bool checkPositions(int p1, int p2, char ch, string password)
        {
            return (password[p1 - 1] == ch) ^ (password[p2 - 1] == ch);
        }

        static IEnumerable<bool> checkPasswords(Func<int, int, char, string, bool> validator)
        {
            Regex r = new Regex(@"^(?<min>[0-9]+)-(?<max>[0-9]+) (?<ch>[a-zA-Z0-9]+): (?<pw>[a-zA-Z0-9]+)$");
            while (true) {
                string line = Console.ReadLine();
                if (line is null) break;
                Match m = r.Match(line);
                Console.WriteLine($"Found {m.Groups.Count} matches");
                if (m.Groups.Count < 5) throw new Exception($"Invalid input: {line}");
                string min = m.Groups["min"].Value;
                string max = m.Groups["max"].Value;
                string ch = m.Groups["ch"].Value;
                string pw = m.Groups["pw"].Value;
                Console.WriteLine($"min: {min}, max: {max}, char: {ch}, pw: {pw}");
                yield return validator(int.Parse(min), int.Parse(max), ch[0], pw);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //Console.WriteLine(checkPasswords(checkCount).Count((v) => { Console.WriteLine(v); return v; }));
            Console.WriteLine(checkPasswords(checkPositions).Count((v) => { Console.WriteLine(v); return v; }));
        }
    }
}
