using System;
using System.Collections.Generic;
using System.Linq;

namespace _5
{
    public static class SeatExtensions
    {
        public static IEnumerable<int> ToSeatId(this IEnumerable<string> seatNumbers)
        {
            return seatNumbers.Select((line) => {
                return line.Select((ch) => {
                    return ch switch {
                      'B' => 1,
                      'R' => 1,
                      'F' => 0,
                      'L' => 0,
                      _ => throw new ArgumentException($"Invalid character {ch}")
                    };
                }).Aggregate(0, (n, b) => ((n << 1) | (b & 0x1)));
            });
        }
    }

    class Program
    {
        static IEnumerable<string> readLines()
        {
            string line = Console.ReadLine();
            while (!(line is null))
            {
                yield return line;
                line = Console.ReadLine();
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            IEnumerable<int> seatIds = readLines().ToSeatId().ToList();
            Console.WriteLine($"> {seatIds.Max()}");
            (int last, int gap) = seatIds.OrderBy((i) => i).Aggregate((0, 0), (agg, v) => {
                (int last, int gap) = agg;
                Console.WriteLine($"A {last}, {gap}, {v}");
                if (gap > 0) return (v, gap);
                if (last > 0 && v - last > 1) return (v, last + 1);
                return (v, gap);
            });
            Console.WriteLine($"+ {gap}");
        }
    }
}
