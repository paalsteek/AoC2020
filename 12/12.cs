using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc
{
    class Instruction
    {
        public enum Direction
        {
            NORTH,
            EAST,
            SOUTH,
            WEST,
            LEFT,
            RIGHT,
            FORWARD
        }

        public Direction Dir { get; }
        public int Value { get; }

        public Instruction(string input)
        {
            Dir = input[0] switch
            {
                'N' => Direction.NORTH,
                'S' => Direction.SOUTH,
                'E' => Direction.EAST,
                'W' => Direction.WEST,
                'L' => Direction.LEFT,
                'R' => Direction.RIGHT,
                'F' => Direction.FORWARD,
                _   => throw new Exception($"Invalid Direction {input[0]}")
            };
            Value = int.Parse(input[1..]);
        }

        public static Direction Turn(Direction start, int degree)
        {
            switch (degree)
            {
                case 0:
                case 360:
                    return start;
                case 90:
                    return (Direction) (((int) start + 1) % 4);
                case 180:
                    return (Direction) (((int) start + 2) % 4);
                case 270:
                    return (Direction) (((int) start + 3) % 4);
                default:
                    throw new Exception("Turning is only supported in 90 Degree steps");
            }
        }
    }

    class Ship
    {
        public int PosNS { get; private set; } = 0;
        public int PosEW { get; private set; } = 0;
        public Instruction.Direction Face { get; private set; } = Instruction.Direction.EAST;

        public void Move(Instruction i)
        {
            Instruction.Direction dir = i.Dir;
            if (dir == Instruction.Direction.FORWARD) dir = Face;
            switch (dir)
            {
                case Instruction.Direction.NORTH:
                    PosNS += i.Value;
                    break;
                case Instruction.Direction.SOUTH:
                    PosNS -= i.Value;
                    break;
                case Instruction.Direction.EAST:
                    PosEW += i.Value;
                    break;
                case Instruction.Direction.WEST:
                    PosEW -= i.Value;
                    break;
                case Instruction.Direction.LEFT:
                    Face = Instruction.Turn(Face, 360 - i.Value);
                    break;
                case Instruction.Direction.RIGHT:
                    Face = Instruction.Turn(Face, i.Value);
                    break;
                default:
                    throw new Exception($"Unknown or unexpected Direction {i.Dir}");
            }
        }

        public override string ToString()
        {
            return $"Ship(NS: {PosNS}, EW: {PosEW}, F: {Face}, MHD: {System.Math.Abs(PosNS) + System.Math.Abs(PosEW)})";
        }
    }

    class Program
    {
        static IEnumerable<string> readValues()
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (string.IsNullOrEmpty(line)) yield break;
                yield return line;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Ship s = readValues().Select((l) => new Instruction(l)).Aggregate(new Ship(), (Ship s, Instruction i) => { Console.WriteLine(s); s.Move(i); Console.WriteLine(s); return s; });
            Console.WriteLine($"Final state: {s}");
        }
    }
}
