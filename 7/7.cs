using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace _7
{
    public static class DictExt
    {
        public static V GetValueOrAdd<V>(this IDictionary<string, V> dict, string key, V v = default)
            where V: notnull, Bag
        {
            if (!dict.TryGetValue(key, out V? b))
            {
                dict.Add(key, v);
                v.Name = key;
                return v;
            }
            return b;
        }
    }

    public class Bag
    {
        private List<(int, Bag)> _Content { get; } = new List<(int, Bag)>();
        private List<Bag> _Parents { get; } = new List<Bag>();

        public string? Name;
        public IReadOnlyCollection<(int, Bag)> Content { get { return _Content; } }
        public IReadOnlyCollection<Bag> Parents { get { return _Parents; } }

        public void AddContent(int count, Bag b)
        {
            _Content.Add((count, b));
            b.AddParent(this);
        }

        public void AddParent(Bag b)
        {
            _Parents.Add(b);
        }

        public IEnumerable<string?> ListParents()
        {
            return _Parents.SelectMany((p) => { return p.ListParents(); }).Append(Name).Distinct();
        }

        public int CountBags()
        {
            return _Content.Select((v) => { return v.Item1*v.Item2.CountBags(); }).Sum() + 1;
        }
    }

    static class Program
    {
        static IEnumerable<string> readLinesAsync()
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (line is null) yield break;
                yield return line;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var x = readLinesAsync().Select((line) => {
                return Regex.Match(line, @"^(?<one>[a-z ]+) bags contain ((?:(?:, )?(?<twon>[0-9]+) (?<two>[a-z ]+) bags?)+|(?<nobags>no other bags|))\.$");
            });
            IDictionary<string, Bag> bags = new Dictionary<string, Bag>();
            foreach (Match m in x)
            {
                string one = m.Groups["one"].Value;
                Bag b = bags.GetValueOrAdd(one, new Bag());
                if (m.Groups["nobags"].Success)
                {
                    //Console.WriteLine($"{one} is a leaf");
                }
                else
                {
                    for (int i = 0; i < m.Groups["twon"].Captures.Count; i++)
                    {
                        string twon = m.Groups["twon"].Captures[i].Value;
                        string two  = m.Groups["two"].Captures[i].Value;
                        //Console.WriteLine($"{one} contains {twon} of {two}");
                        b.AddContent(int.Parse(twon), bags.GetValueOrAdd(two, new Bag()));
                    }
                }
            }
            Console.WriteLine($"Found {bags.Count} bags.");
            foreach ((string key, Bag b) in bags)
            {
                //Console.WriteLine($"Bag {key} contains {b.Content.Count} bags.");
            }
            Console.WriteLine($"-> {bags["shiny gold"].ListParents().Count() - 1}");
            Console.WriteLine($"=> {bags["shiny gold"].CountBags() - 1}");
        }
    }
}
