using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Aoc
{
    class Command
    {
        public enum Instruction {
            NOP,
            ACC,
            JMP
        }
        public Instruction Op  { get; }
        public int         Arg { get; }

        public Command(Instruction o, int a)
        {
            Op = o;
            Arg = a;
        }

        public Command(string line)
        {
            Op = line.Substring(0, 3) switch {
                "nop" => Instruction.NOP,
                "acc" => Instruction.ACC,
                "jmp" => Instruction.JMP,
                    _ => throw new NotImplementedException()
            };
            Arg = int.Parse(line.Substring(4));
        }

        public static explicit operator Command(string line) => new Command(line);
    }

    class Program
    {
        IList<Command> Code { get; set; } = new List<Command>();
        int PC  { get; set; } = 0;
        int Acc { get; set; } = 0;

        void Step()
        {
            Command current = Code[PC];
            if (current.Op == Command.Instruction.ACC)
            {
                Acc += current.Arg;
            }
            if (current.Op == Command.Instruction.JMP)
            {
                PC += current.Arg;
            }
            else
            {
                PC++;
            }
        }

        void Run(CancellationToken cancellationToken = default)
        {
            ISet<int> loopDetect = new SortedSet<int>();
            loopDetect.Add(PC);
            while (PC < Code.Count && !cancellationToken.IsCancellationRequested)
            {
                Step();
                if (loopDetect.Contains(PC))
                {
                    break;
                }
                loopDetect.Add(PC);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            List<Command> code = new List<Command>();
            string line;
            while ((line = Console.ReadLine()) != null)
            {
                try
                {
                    code.Add((Command) line);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Invalid line: {e}");
                }
            }
            Console.WriteLine($"Got {code.Count} lines of code.");
            bool isNopOrJmp((Command c, int i) v) => v.c.Op == Command.Instruction.NOP || v.c.Op == Command.Instruction.JMP;
            ICollection<Task> tasks = new List<Task>();
            CancellationTokenSource token = new CancellationTokenSource();
            foreach((Command c, int i) in code.Select((c, i) => (c, i)).Where(isNopOrJmp))
            {
                Task t = Task.Run(() => {
                    Program p = new Program()
                    {
                        Code = new List<Command>(code)
                    };
                    // Create a new Command as we did not make a deep copy of Code with NOP changed to JMP or JMP changed to NOP
                    p.Code[i] = new Command(
                        p.Code[i].Op switch
                        {
                            Command.Instruction.NOP => Command.Instruction.JMP,
                            Command.Instruction.JMP => Command.Instruction.NOP,
                            _ => throw new Exception()
                        },
                        p.Code[i].Arg
                    );
                    p.Run(token.Token);
                    if (p.PC >= p.Code.Count)
                    {
                        Console.WriteLine($"Final Acc: {p.Acc}");
                        token.Cancel();
                    }
                });
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
        }
    }
}
